import Coupon from "../src/Coupon"
import Item from "../src/Item"
import Order from "../src/Order"

test("Não deve criar um pedido com cpf invalido", function () {
  expect(() => new Order("935.411.347-81")).toThrow(new Error("CPF Inválido"))
})

test("Deve criar um pedido com 3 itens", function () {
  const order = new Order("935.411.347-80")
  order.addItem(new Item(1, "Instrumentos Musicais", "Guitarra", 1000), 1)
  order.addItem(new Item(3, "Instrumentos Musicais", "Amplifcador", 5000), 1)
  order.addItem(new Item(3, "Instrumentos Musicais", "Guitarra", 30), 3)
  const total = order.getTotal()
  expect(total).toBe(6090)
})

test("Deve criar um pedido com 3 itens com cupom de desconto", function () {
  const order = new Order("935.411.347-80")
  order.addItem(new Item(1, "Instrumentos Musicais", "Guitarra", 1000), 1)
  order.addItem(new Item(3, "Instrumentos Musicais", "Amplifcador", 5000), 1)
  order.addItem(new Item(3, "Instrumentos Musicais", "Guitarra", 30), 3)
  const coupon = new Coupon("VALE20", 20, new Date ())
  order.addCoupon(coupon)
  const total = order.getTotal();
  expect(total).toBe(4872)
})

