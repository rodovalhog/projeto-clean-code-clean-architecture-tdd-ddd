import Coupon from "../src/Coupon"

test("Deve criar um cupom de desconto", () => {
  const coupon = new Coupon("VALE20", 20, new Date("2021-03-01T10:00:00"))
  expect(coupon.percentage).toBe(20)
  const isExpired = coupon.isExpired() 
  expect(isExpired).toBeFalsy()
})

test("Deve criar um cupom de desconto expirado", () => {
  const coupon = new Coupon("VALE20", 20, new Date("2021-03-01T10:00:00"))
  const isExpired = coupon.isExpired()
  expect(isExpired).toBeTruthy() 
})