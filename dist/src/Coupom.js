"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Coupon {
    constructor(code, percentage, data) {
        this.code = code;
        this.percentage = percentage;
        this.data = data;
    }
    getPercentage() {
        return this.percentage;
    }
}
exports.default = Coupon;
