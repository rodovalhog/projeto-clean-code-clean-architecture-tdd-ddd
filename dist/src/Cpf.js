"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cpf {
    constructor(value) {
        this.FACTOR_DIGITS_1 = 10;
        this.FACTOR_DIGITS_2 = 11;
        if (!this.validateCPF(value))
            throw new Error("CPF Inválido");
        this.value = value;
    }
    getValue() {
        return this.value;
    }
    validateCPF(cpf) {
        if (!cpf)
            return false;
        cpf = this.cleanCpf(cpf);
        if (!this.isValidLength(cpf))
            return false;
        if (this.hasAllDigitsEqual(cpf))
            return false;
        const dg1 = this.calculateCheckDigit(cpf, this.FACTOR_DIGITS_1);
        const dg2 = this.calculateCheckDigit(cpf, this.FACTOR_DIGITS_2);
        let checkDigits = this.extractCheckDigits(cpf);
        const caluculetedDigit = `${dg1}${dg2}`;
        return checkDigits == caluculetedDigit;
    }
    cleanCpf(cpf) {
        return cpf.replace(/[\.\-]/g, "");
    }
    isValidLength(cpf) {
        return cpf.length === 11;
    }
    hasAllDigitsEqual(cpf) {
        const [firstDigits] = cpf;
        return [...cpf].every(digit => digit === firstDigits);
    }
    calculateCheckDigit(cpf, factor) {
        let total = 0;
        for (const digit of cpf) {
            if (factor > 1)
                total += parseInt(digit) * factor--;
        }
        const rest = total % 11;
        return (rest < 2) ? 0 : (11 - rest);
    }
    extractCheckDigits(cpf) {
        return cpf.slice(-2);
    }
}
exports.default = Cpf;
/*
Encapsulamento gera baixo acoplamento
ele protege o estado do objeto
*/ 
