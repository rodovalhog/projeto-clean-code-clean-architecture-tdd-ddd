"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Cpf_1 = __importDefault(require("../src/Cpf"));
test("Deve testar um cpf valido", function () {
    const numeroCPF = "935.411.347-80";
    const cpf = new Cpf_1.default(numeroCPF);
    expect(cpf.getValue()).toBe(numeroCPF);
});
//Veja que nesse teste ele não cria uma instancia da class Cpf isso porque
//a instancia e criada na arrow function que esta sendo chamada dentro do expect
const invalidCpfWithSameDigits = ['111.111.111-11', '222.222.222-22', "333.333.333-33"];
describe.each(invalidCpfWithSameDigits)("Deve testar um cpf invalido com os digitos iguais", (cpf) => {
    test(`${cpf}`, function () {
        expect(() => new Cpf_1.default(cpf)).toThrow(new Error("CPF Inválido"));
    });
});
test("Deve testar um cpf invalido'com digitos diferentes", function () {
    const numeroCPF = "123.456.789-99";
    expect(() => new Cpf_1.default(numeroCPF)).toThrow(new Error("CPF Inválido"));
});
