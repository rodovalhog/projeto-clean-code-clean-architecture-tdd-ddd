"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Coupon_1 = __importDefault(require("../src/Coupon"));
test("Deve criar um cupom de desconto", () => {
    const coupon = new Coupon_1.default("VALE20", 20, new Date());
    expect(coupon.percentage).toBe(20);
    const isExpired = coupon.isExpired();
    expect(isExpired).toBeFalsy();
});
test("Deve criar um cupom de desconto expirado", () => {
    const coupon = new Coupon_1.default("VALE20", 20, new Date("2021-03-01T10:00:00"));
    const isExpired = coupon.isExpired();
    expect(isExpired).toBeTruthy();
});
