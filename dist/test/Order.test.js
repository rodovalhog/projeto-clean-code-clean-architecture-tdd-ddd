"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Coupon_1 = __importDefault(require("../src/Coupon"));
const Item_1 = __importDefault(require("../src/Item"));
const Order_1 = __importDefault(require("../src/Order"));
test("Não deve criar um pedido com cpf invalido", function () {
    expect(() => new Order_1.default("935.411.347-81")).toThrow(new Error("CPF Inválido"));
});
test("Deve criar um pedido com 3 itens", function () {
    const order = new Order_1.default("935.411.347-80");
    order.addItem(new Item_1.default(1, "Instrumentos Musicais", "Guitarra", 1000), 1);
    order.addItem(new Item_1.default(3, "Instrumentos Musicais", "Amplifcador", 5000), 1);
    order.addItem(new Item_1.default(3, "Instrumentos Musicais", "Guitarra", 30), 3);
    const total = order.getTotal();
    expect(total).toBe(6090);
});
test("Deve criar um pedido com 3 itens com cupom de desconto", function () {
    const order = new Order_1.default("935.411.347-80");
    order.addItem(new Item_1.default(1, "Instrumentos Musicais", "Guitarra", 1000), 1);
    order.addItem(new Item_1.default(3, "Instrumentos Musicais", "Amplifcador", 5000), 1);
    order.addItem(new Item_1.default(3, "Instrumentos Musicais", "Guitarra", 30), 3);
    const coupon = new Coupon_1.default("VALE20", 20, new Date());
    order.addCoupon(coupon);
    const total = order.getTotal();
    expect(total).toBe(4872);
});
