export default class Cpf {
  private FACTOR_DIGITS_1 = 10
  private FACTOR_DIGITS_2 = 11
  private value: string
  
  constructor (value: string) {
    if(!this.validateCPF(value)) throw new Error ("CPF Inválido")
    this.value = value
  }

getValue () {
  return this.value
}

private validateCPF (cpf: string) {
    if (!cpf) return false
    cpf = this.cleanCpf(cpf)
    if (!this.isValidLength(cpf)) return false
    if (this.hasAllDigitsEqual(cpf)) return false
    const dg1 = this.calculateCheckDigit(cpf, this.FACTOR_DIGITS_1)
    const dg2 = this.calculateCheckDigit(cpf, this.FACTOR_DIGITS_2)
    let checkDigits = this.extractCheckDigits(cpf);  
    const caluculetedDigit = `${dg1}${dg2}`;  
    return checkDigits == caluculetedDigit;
}

private cleanCpf (cpf: string) {
    return cpf.replace(/[\.\-]/g, "")
}

private isValidLength (cpf: string) {
    return cpf.length === 11
}

private hasAllDigitsEqual (cpf: string) {
    const [firstDigits] = cpf 
    return [...cpf].every(digit => digit === firstDigits)
}

private calculateCheckDigit (cpf: string, factor: number) {
    let total = 0
    for (const digit of cpf) {
        if(factor > 1) total += parseInt(digit) * factor--;
    }
    const rest = total%11
    return (rest < 2) ? 0 : (11 - rest)
}

private extractCheckDigits (cpf: string) {
    return cpf.slice(-2)
}

}
/*
Encapsulamento gera baixo acoplamento
ele protege o estado do objeto
*/