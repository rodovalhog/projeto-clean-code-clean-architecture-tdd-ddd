export default class Coupon {
  constructor (readonly code: string, readonly percentage: number, readonly expiredDate?: Date) {
  }

 isExpired () {
  if(!this.expiredDate) return false
  const today = new Date()
  return this.expiredDate.getTime() < today.getTime()
 }
}